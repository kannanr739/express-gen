const express=require('express');
const mongoose=require('mongoose');
const bodyparser=require('body-parser');
const promotions=require('../models/promotions');
var authenticate = require('../authenticate');
const cors = require('./cors');
const promoRouter=express.Router();
promoRouter.use(bodyparser.json());

promoRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get( cors.cors,(req,res,next) => {
  promotions.find({})
  .then((promotion)=>{
    res.statusCode=200;
    res.setHeader('Content-type','application/json');
    res.json(promotion);
  },(err)=>next(err))
  .catch((err)=>next(err));
})
.post(cors.corsWithOptions,authenticate.verifyUser,  (req, res, next) => {
  promotions.create(req.body)
  .then((dish)=>{
    console.log('promotion Created',dish);
       res.statusCode=200;
       res.setHeader('Content-type','application/json');
       res.json(req.body);
  },(err)=>next(err))
  .catch((err)=>next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /promotion');
})
.delete(cors.corsWithOptions,authenticate.verifyUser,  (req, res, next) => {
  promotions.remove({})
  .then((resp)=>{
    console.log('promotions deleted',resp);
       res.statusCode=200;
       res.setHeader('Content-type','application/json');
       res.json(resp);
  },(err)=>next(err))
  .catch((err)=>next(err));
});


promoRouter.route('/:promotionId')
.get( cors.cors,(req,res,next) => {
  
    promotions.findById(req.params.promotionId)
    .then((promotion) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /promotion/'+ req.params.promotionId);
})
.put( cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
  promotions.findByIdAndUpdate(req.params.promotionId, {
    $set: req.body
}, { new: true })
.then((promotion) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(promotion);
}, (err) => next(err))
.catch((err) => next(err));
})
.delete(cors.corsWithOptions,authenticate.verifyUser,  (req, res, next) => {
 promotions.findByIdAndRemove(req.params.promotionId)
  .then((resp) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(resp);
  }, (err) => next(err))
  .catch((err) => next(err));
});


module.exports=promoRouter;