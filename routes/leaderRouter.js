const express=require('express');
const mongoose=require('mongoose');
const bodyparser=require('body-parser');
const leaders=require('../models/leaders');
var authenticate = require('../authenticate');
const cors = require('./cors');
const leaderRouter=express.Router();
leaderRouter.use(bodyparser.json());

leaderRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req,res,next) => {
  leaders.find({})
  .then((leader)=>{
    res.statusCode=200;
    res.setHeader('Content-type','application/json');
    res.json(leader);
  },(err)=>next(err))
  .catch((err)=>next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  leaders.create(req.body)
  .then((dish)=>{
    console.log('Leader Created',dish);
       res.statusCode=200;
       res.setHeader('Content-type','application/json');
       res.json(req.body);
  },(err)=>next(err))
  .catch((err)=>next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /Leader');
})
.delete( cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
  leaders.remove({})
  .then((resp)=>{
    console.log('leaders deleted',resp);
       res.statusCode=200;
       res.setHeader('Content-type','application/json');
       res.json(resp);
  },(err)=>next(err))
  .catch((err)=>next(err));
});


leaderRouter.route('/:LeaderId')
.get( cors.cors,(req,res,next) => {
  
    leaders.findById(req.params.LeaderId)
    .then((leader) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(leader);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /Leader/'+ req.params.LeaderId);
})
.put(cors.corsWithOptions,authenticate.verifyUser,  (req, res, next) => {
  leaders.findByIdAndUpdate(req.params.LeaderId, {
    $set: req.body
}, { new: true })
.then((leader) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(leader);
}, (err) => next(err))
.catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
 leaders.findByIdAndRemove(req.params.LeaderId)
  .then((resp) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(resp);
  }, (err) => next(err))
  .catch((err) => next(err));
});


module.exports=leaderRouter;